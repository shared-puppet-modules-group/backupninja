# Run duplicity as part of a backupninja run.
#
define backupninja::action::dup (
  String $ensure                                 = 'present',
  Integer $order                                 = 90,
  Optional[Boolean] $validate                    = undef,
  # general handler config
  Optional[String] $when                         = undef,
  Optional[String] $options                      = undef,
  Integer $nicelevel                             = 0,
  Boolean $testconnect                           = true,
  Stdlib::Absolutepath $tmpdir                   = '/tmp',
  # [gpg]
  Boolean $sign                                  = false,
  Optional[String] $encryptkey                   = undef,
  Optional[String] $signkey                      = undef,
  Optional[String] $password                     = undef,
  Optional[String] $signpassword                 = undef,
  # [source]
  Optional[Array[String]] $include               = undef,
  Optional[Array[String]] $exclude               = undef,
  # [dest]
  Boolean $incremental                           = true,
  Variant[Enum['keep'], Integer] $increments     = 30,
  Variant[Enum['yes'], Integer] $keep            = 60,
  Variant[Enum['all'], Integer] $keepincroffulls = 'all',
  Optional[String] $desturl                      = undef,
  Optional[String] $awsaccesskeyid               = undef,
  Optional[String] $awssecretaccesskey           = undef,
  Optional[String] $cfusername                   = undef,
  Optional[String] $cfapikey                     = undef,
  Optional[String] $cfauthurl                    = undef,
  Optional[String] $dropboxappkey                = undef,
  Optional[String] $dropboxappsecret             = undef,
  Optional[String] $dropboxaccesstoken           = undef,
  Optional[String] $ftp_password                 = undef,
  Integer $bandwidthlimit                        = 0,
  Optional[String] $sshoptions                   = undef,
  Stdlib::Absolutepath $destdir                  = "${backupninja::real_account_home}/${name}",
  Optional[String] $desthost                     = $backupninja::account_host,
  Optional[String] $destuser                     = $backupninja::account_user,
) {

  # install client dependencies
  if $backupninja::manage_packages {
    ensure_packages(['duplicity'], {'ensure' => $backupninja::ensure_duplicity_version})
    if $bandwidthlimit {
      ensure_packages(['trickle'], {'ensure' => 'installed'})
    }
  }

  if empty($desturl) {

    if empty($desthost) or empty($destuser) {
      fail('Must define desthost and destuser!')
    }

    # $backupninja::account_home was empty and default used
    if $destdir == "/${name}" {
      fail('Must define a destdir for backup!')
    }

    $_desthost = $desthost
    $_destuser = $destuser
    $_destdir = $destdir
  }
  else {
    $_desthost = undef
    $_destuser = undef
    $_destdir = undef
  }

  if empty($include) {
    fail('Must include one or more directories to backup!')
  }

  $config = {
    'when'        => $when,
    'options'     => $options,
    'nicelevel'   => $nicelevel,
    'testconnect' => $testconnect,
    'tmpdir'      => $tmpdir,
    'gpg'         => {
      'sign'         => $sign,
      'encryptkey'   => $encryptkey,
      'signkey'      => $signkey,
      'password'     => $password,
      'signpassword' => $signpassword,
    },
    'source'      => {
      'include' => $include,
      'exclude' => $exclude,
    },
    'dest'        => {
      'incremental'        => $incremental,
      'increments'         => $increments,
      'keep'               => $keep,
      'keepincroffulls'    => $keepincroffulls,
      'desturl'            => $desturl,
      'awsaccesskeyid'     => $awsaccesskeyid,
      'awssecretaccesskey' => $awssecretaccesskey,
      'cfusername'         => $cfusername,
      'cfapikey'           => $cfapikey,
      'cfauthurl'          => $cfauthurl,
      'dropboxappkey'      => $dropboxappkey,
      'dropboxappsecret'   => $dropboxappsecret,
      'dropboxaccesstoken' => $dropboxaccesstoken,
      'ftp_password'       => $ftp_password,
      'bandwidthlimit'     => $bandwidthlimit,
      'destdir'            => $_destdir,
      'desthost'           => $_desthost,
      'destuser'           => $_destuser,
      'sshoptions'         => $sshoptions,
    },
  }

  backupninja::action { $name:
    ensure   => $ensure,
    order    => $order,
    type     => 'dup',
    content  => epp('backupninja/action.epp', { 'config' => $config }),
    validate => $validate,
  }

}
