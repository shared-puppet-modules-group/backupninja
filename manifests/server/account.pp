# This defined type allows nodes to declare a remote account to upload backups,
# leveraging exported ressources (PuppetDB).
#
# By default, a node for which `$backupninja::server` is set to `true` will
# automatically realize all backupninja::server:account resources which are
# tagged with its own FQDN.
#
# @see backupninja::server
#
# @param ensure
#   Set this to 'absent' to remove all traces of the backup account on the
#   server. Doing this will also remove all backup data for this user.
#
# @param user
#   Specifies the username of the account.
#
# @param home
#   Specifies the home directory of the account.
#
# @param gid
#   Specifies the gid of the account.
#
# @param manage_homedir
#   Whether to manage the home directory.
#
# @param manage_sshdir
#   Whether to manage the SSH configuration directory.
#
# @param manage_sshkey
#   Whether to manage the public key in the account's authorized_keys.
#
# @param purge_ssh_keys
#   Set this to true to have all unknown ssh keys removed in the account's
#   authorized_keys file. Enabling this without setting manage_sshkey to true
#   might result in an empty authorized_keys file, and thus access from the
#   client machine will be impossible. In such a case, if you want to keep
#   some ssh keys around make sure to manage them somewhere with an
#   ssh_authorized_key resource.
#
# @param sshdir
#   Specifies the path to the user account's SSH configuration directory.
#
# @param sshfile
#   Specifies the name of the user account's SSH authorized_key file.
#
# @param sshkey
#   Contains an SSH public key to be deployed in the user account's
#   ssh_authorized_keys file.
#
# @param sshkey_type
#   Specifies the type of the SSH public key defined the the above parameter.
#
# @param sshkey_options
#   Specifies the ssh_authorized_keys options attached to the SSH public key.
#
# @param sshkey_source
#   Specifies a source path for the account's ssh_authorized_keys file. This
#   parameter has no effect if sshkey is defined, and causes sshkey_type and
#   sshkey_options parameters to be ignored.
#
# @param uid
#   Specifies the account's uid.
#
# @example
#   @@backupninja::server::account {
#     "${::fqdn}@backup.example.net":
#       user          => "backup-${::hostname}",
#       home          => "/data/backups/${::fqdn}",
#       sshkey_source => "puppet:///modules/profile/ssh_keys/${::fqdn}.pub",
#       tag           => 'backup.example.net',
#   }
#
define backupninja::server::account (
  String $user,
  String $home,
  String $gid,
  Enum['present','absent'] $ensure                         = 'present',
  Boolean $manage_homedir                                  = true,
  Boolean $manage_sshdir                                   = true,
  Boolean $manage_sshkey                                   = true,
  Boolean $purge_ssh_keys                                  = false,
  Optional[String] $sshdir                                 = "${home}/.ssh",
  Optional[String] $sshfile                                = 'authorized_keys',
  Optional[String] $sshkey                                 = undef,
  Optional[String] $sshkey_type                            = undef,
  Optional[Variant[String, Array[String]]] $sshkey_options = undef,
  Optional[String] $sshkey_source                          = undef,
  Optional[Integer] $uid                                   = undef,
) {

  if $ensure == 'present' {
    $dir_ensure = 'directory'
  }
  else {
    $dir_ensure = $ensure
  }

  user { $user:
    ensure         => $ensure,
    uid            => $uid,
    gid            => $gid,
    comment        => "${user} backup sandbox",
    home           => $home,
    managehome     => true,
    purge_ssh_keys => $purge_ssh_keys,
    shell          => '/bin/bash',
    password       => '*',
    require        => Group[$gid],
  }

  if $manage_homedir {
    file { $home:
      ensure => $dir_ensure,
      mode   => '0750',
      owner  => $user,
      group  => 'root',
    }
  }

  if $manage_sshdir {
    file { $sshdir:
      ensure => $dir_ensure,
      mode   => '0700',
      owner  => $user,
      group  => $gid,
    }
  }

  if $manage_sshkey {

    if empty($sshkey) and empty($sshkey_source) {
      fail('Either sshkey or sshkey_source must be defined.')
    }

    if $sshkey {

      ssh_authorized_key{ $user:
        type    => $sshkey_type,
        options => $sshkey_options,
        key     => $sshkey,
        user    => $user,
        target  => "${sshdir}/${sshfile}",
        require => [ User[$user], File[$sshdir] ],
      }

    }
    else {

      file { "${sshdir}/${sshfile}":
        ensure => $ensure,
        mode   => '0644',
        owner  => 'root',
        group  => 'root',
        source => $sshkey_source,
      }

    }

  }

}
