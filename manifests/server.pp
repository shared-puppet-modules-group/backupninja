# This class configures server resources for hosting remote backups.
#
# @private
#
class backupninja::server {

  assert_private()

  group { $backupninja::account_group:
    ensure => 'present',
    gid    => $backupninja::account_gid,
  }
  -> file { $backupninja::server_backupdir:
    ensure => $backupninja::server_backupdir_ensure,
    target => $backupninja::server_backupdir_target,
    mode   => $backupninja::server_backupdir_mode,
    owner  => 'root',
    group  => $backupninja::account_group,
  }

  # collect all resources from hosted backups
  Backupninja::Server::Account <<| tag == $facts['networking']['fqdn'] |>>

}
