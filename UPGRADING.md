## UPGRADING

This document is intended for users of the pre-4.0.0 (legacy) version of the shared-puppet-modules-group/backupninja module, and describes the changes to note when switching to the current version of the module.

### General

- The default module parameter values now are stored in hiera, see `data/common.yaml`
- The [module documentation](https://shared-puppet-modules-group.gitlab.io/backupninja/) is now generated automatically using Puppet String and GitLab CI
- The module is also now automatically checked for lint and syntax errors, using GitLab CI
- Parameters and resources related to vservers and nagios have been removed
- "sandboxes" are now "accounts"

### Classes

#### backupninja

This class is still responsible for installing and configuring backupninja. Most of this class' parameters are directly mapped to backupninja configuration options.

- Some parameters have been renamed to conform with the actual backupninja configuration option (eg. `configdir` -> `configdirectory`
- Some missing backupninja config options were added (eg. `reportspace`)
- New parameters were added to manage the default backupninja cron job (eg. `manage_cron`)
- The `purge_configdirectory` option was introduced
- A `validate_actions` parameter was introduced, to toggle automatic testing of backupninja actions

SSH key handling has been revamped. While the legacy version attempted to manage multiple sandboxes, multiple usernames and multiple keys, the new version by default only manages one remote account on one backup server. That account is configured using the `account_*` parameters of the backupninja class. All `key*` parameters from the legacy version have been removed.

- `keymanage` -> `manage_account_sshkey`
- `keytype` -> `account_sshkey_type`
- `keystore` -> `account_sshkey_source` (the full URL to the key is now required, not just a base path)
- `keydest`, `keyowner`, `keygroup`, `keystorefspath` -> removed, module doesn't generate SSH keys anymore

In the case where more than one remote account is required, extra `backupninja::server::account` resources may be declared.

#### backupninja::server

- We now use a toggle parameter in the main backupninja class (`server`) to determine whether to include this class
- `backupdir` parameter is now called `server_backupdir` in the main class

### Defined types

#### backupninja::key and backupninja::generate_ssh_key

Facilities to generate SSH keys has been removed. The recommended alternative is to use the [voxpupuli/puppet-ssh_keygen](https://github.com/voxpupuli/puppet-ssh_keygen) to generate an SSH keypair as part of managing the root account on client nodes.

#### backupninja::server::sandbox and backupninja::server::backupninja_server_realize

This defined type has been basically rewritten, and its basic functionality is now provided by the `backupninja::server::account` class. Handler resources do not declare or export any account-related resources anymore. This is now done through `backupninja`, using `account_*` parameters. Handlers which require configuration parameters such as remote host or username, use as default values those set in `backupninja`.

#### backupninja::cron

The default cron job is now managed by default though the new `backupninja::cron` class.

Extra cron jobs for backupninja may now be deployed using the `backupninja::cron::task` defined type.

#### backupninja::\* actions

A new namespace within the module has been created for backup actions: `backupninja::action::*`

- Boolean parameters are now really Boolean (eg. true instead of 'yes')
- Default values match the actual handler defaults (unless in cases where it doesn't make sense)
- These defined types do not cause "sandboxes" resources to be exported anymore, and so several account-related parameters have been removed

A new `backupninja::action` defined type has been added, to allow deploying arbitrary backup actions.

### Templates

The backupninja configuration file template has been rewritten in EPP format and is now based on the the default, commented config file.

Handlers-specific templates have been scrapped. One EPP template is now used for all handler configuration files.
